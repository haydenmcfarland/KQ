# King's Quest

### Summary:

A 2D platformer centered around manipulating and exploding clones developed in Unity with C#.

[![gameplay](https://haydenmcfarland.github.io/haydenmcfarland/images/kq.gif)](https://www.youtube.com/watch?v=nfwTiWnE6ZQ)

[Download](https://haydenmcfarland.github.io/haydenmcfarland/downloads/kq.zip)
